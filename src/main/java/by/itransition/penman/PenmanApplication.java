package by.itransition.penman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PenmanApplication {

    public static void main(String[] args) {
        SpringApplication.run(PenmanApplication.class, args);
    }

}
